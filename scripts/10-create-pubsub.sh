#!/bin/sh
set -e
HERE=$(dirname $(readlink -f "$0"))
source "${HERE}/constants.sh"
gcloud pubsub topics create "$PUBSUB_TOPIC_NAME"
gcloud scheduler jobs create pubsub "$SCHED_JOB_NAME" --schedule="0 7 * * *" --topic="$PUBSUB_TOPIC_NAME" --message-body="hj"

