#!/bin/sh
set -e
HERE=$(dirname $(readlink -f "$0"))
source "${HERE}/constants.sh"
gcloud iam service-accounts create "$SERVICEACCOUNT_NAME" --description "KiongaHotjava Periodic Function"

