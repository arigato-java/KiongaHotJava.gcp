#!/bin/sh
set -e
HERE=$(dirname $(readlink -f "$0"))
. "${HERE}/constants.sh"

gcloud functions deploy "$FN_NAME" --region="$REGION" \
	--entry-point=main \
	--memory=128MB \
	--runtime=nodejs14 \
	--source="$HERE/../src" \
	--timeout=15s \
	--env-vars-file="$HERE/../secrets.yaml" \
	--trigger-topic="$PUBSUB_TOPIC_NAME" \
	--service-account "$SERVICEACCOUNT_NAME@$(gcloud config list --format 'value(core.project)' 2>/dev/null).iam.gserviceaccount.com"

