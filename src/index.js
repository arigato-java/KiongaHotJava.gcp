'use strict';
const axios=require('axios');
const readline=require('readline');
const Twitter=require('twitter');

function KiongaHotJava(options){
	// options { WEATHER_CITY_ID: string of JMA location id, twitter: obj twitter credentials}
	this.options=options;
}
KiongaHotJava.prototype.run=function(){
	return this.acquireForecastData()
		.then(this.gotWeather.bind(this));
};
KiongaHotJava.prototype.acquireForecastData=function(){
	return axios({	method: 'get',
			url: 'https://www.data.jma.go.jp/obd/stats/data/mdrr/tem_rct/alltable/mxtemsadext00_rct.csv',
            validateStatus: function (status) {
                return status === 200;
            },
			responseType: 'stream'
		}
	).then((response)=>{
		// search temperature
		return (async function(cityid){
			const rl=readline.createInterface({
				input: response.data,
				crlfDelay: Infinity
			});
            const line_search_prefix=cityid+',';
			for await (const line of rl){
				if(!line.startsWith(line_search_prefix)){
                    continue;
                }
				return line;
			}
		})(this.options.WEATHER_CITY_ID)
		.then((location_line)=>{
			const columns=location_line.split(',');
			const temp=Number(columns[9]);
			const datetime=columns.slice(4,7).join('-');
            const res={ temp: temp, date: datetime };
            console.log(JSON.stringify(res));
			return res;
		});
	});
};
KiongaHotJava.prototype.gotWeather=function(response){
	var java_msg=this.selectJavaMessage(response.temp);
	if(!java_msg.length){
		return;
	}
	var post_message=java_msg+response.date;
	return this.postResult(post_message);
};
KiongaHotJava.prototype.selectJavaMessage=function(forTemperatureCelsius){
	const java_tbl=[
		[28,'今日の気温がHotJava'],
		[35,'今日の気温はモットジャバ'],
		[37,'今日も気温がモットジャバ']];
	var res="";
	java_tbl.forEach((tpl)=>{res=(tpl[0]<=forTemperatureCelsius)?tpl[1]+" ":res;});
	return res;
};
KiongaHotJava.prototype.postResult=function(msg){
	var client=new Twitter(this.options.twitter);
	return client.post('statuses/update',{'status':msg});
    //console.log(msg);
    //return new Promise((resolve)=>resolve());
};
function main(params){
	var khj=new KiongaHotJava({
	WEATHER_CITY_ID:params.WEATHER_CITY_ID,
	'twitter':{
		'consumer_key':params.TWITTER_CONSUMER_KEY,
		'consumer_secret':params.TWITTER_CONSUMER_SECRET,
		'access_token_key':params.TWITTER_ACCESS_TOKEN_KEY,
		'access_token_secret':params.TWITTER_ACCESS_TOKEN_SECRET
	}
	});
	return khj.run().catch(console.log);
}

exports.main=(event,context)=>{
	(async function(){await main({
		'WEATHER_CITY_ID': process.env.WEATHER_CITY_ID,
		'TWITTER_CONSUMER_KEY': process.env.TWITTER_CONSUMER_KEY,
		'TWITTER_CONSUMER_SECRET': process.env.TWITTER_CONSUMER_SECRET,
		'TWITTER_ACCESS_TOKEN_KEY': process.env.TWITTER_ACCESS_TOKEN_KEY,
		'TWITTER_ACCESS_TOKEN_SECRET': process.env.TWITTER_ACCESS_TOKEN_SECRET
	});})();
};

//// Debug
//exports.main({},{});
